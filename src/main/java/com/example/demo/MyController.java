package com.example.demo;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyController {
	
	@GetMapping(path="/view", produces = "application/json")
    public String getIndex() 
    {
		return "Welcome to my example2 "+System.currentTimeMillis();
    }
	
	@GetMapping(path="/", produces = "application/json")
    public String getBasicIndex() 
    {
		return "Welcome to my example "+System.currentTimeMillis();
    }

}
